@php
use App\Models\Instant;
use App\Models\Instantend;
use App\Models\Lottery;
use App\Models\Payzone;
use App\Models\Tillerror;
$instants = App\Models\Instant::all();
$instantends = App\Models\Instantend::all();
$lotteries = App\Models\Lottery::all();
$payzones = App\Models\Payzone::all();
$tillerrors = App\Models\Tillerror::all();
{{--$instants=m_Instants::where('active',1)->where('deleted',0)->paginate(1);--}}
@endphp
@extends('layouts.blank')
@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush
@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="/form_data/instant">
                    <div class="tile-stats">
                    <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                    <div class="count">
                        {{ $instants->count() }}
                    </div>
                    <h3>Instant start</h3>
                    </div>
                </a>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="/form_data/instantend">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                    <div class="count">
                        {{ $instantends->count() }}
                    </div>
                    <h3>Instant end</h3>
                </div>
                    </a>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="/form_data/lottery">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                    <div class="count">
                        {{ $lotteries->count() }}
                    </div>
                    <h3>Lottery</h3>
                </div>
                    </a>
            </div>
            <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="/form_data/payzone">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-check-square-o"></i></div>
                    <div class="count">
                        {{ $payzones->count() }}
                    </div>
                    <h3>Payzone</h3>
                </div>
                    </a>
            </div>
            <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="/form_data/tillerror">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-check-square-o"></i></div>
                    <div class="count">
                        {{ $tillerrors->count() }}
                    </div>
                    <h3>Till errors</h3>
                </div>
                    </a>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">

        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
@endsection