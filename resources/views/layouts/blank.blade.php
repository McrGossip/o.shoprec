<!DOCTYPE html>
<html lang="en">

    @include('includes/header')

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

            </div>
        </div>

        @include('includes/footer')

    </body>
</html>