<?php

use App\Models\Payzone;

$payzones = App\Models\Payzone::all();
?>

@extends('layouts.blank')

@push('stylesheets')
        <!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="tile-stats">
                <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                <div class="count">{{ $payzones->count() }}</div>
                <h3>Payzone</h3>


            </div>
        </div>

    </div>
    <div class="row top_tiles">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; font-weight: bold">

            <table class="table .table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>t1 start</th>
                    <th>t1 end</th>
                    <th>t2 start</th>
                    <th>t2 start</th>
                    <th>Submitted</th>
                </tr>
                </thead>
                @foreach ($payzones as $payzone)
                    <tbody>
                    <tr>
                        <td>{{ $payzone->id }}</td>
                        <td>{{ $payzone->employee_name }}</td>
                        <td>{{ $payzone->till_1_start }}</td>
                        <td>{{ $payzone->till_1_end }}</td>
                        <td>{{ $payzone->till_2_start }}</td>
                        <td>{{ $payzone->till_2_end }}</td>
                        <td>{{ $payzone->created_at }}</td>
                    </tr>
                    </tbody>
                @endforeach
            </table>

        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">

    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection