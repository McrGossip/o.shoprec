@php
use App\Models\Instant;

$instants = App\Models\Instant::all();
{{--$instants=m_Instants::where('active',1)->where('deleted',0)->paginate(1);--}}
@endphp

@extends('layouts.blank')

@push('stylesheets')
        <!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <div class="count">
                            {{ $instants->count() }} ~ Instant start</div>
                </div>
        </div>
</div>
        <div class="row top_tiles">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; font-weight: bold">

                <table class="table .table-responsive">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>t1</th>
                        <th>t2</th>
                        <th>Submitted</th>
                    </tr>
                    </thead>
                    @foreach ($instants as $instant)
                    <tbody>
                    <tr>
                        <td>{{ $instant->id }}</td>
                        <td>{{ $instant->employee_name }}</td>
                        <td>{{ $instant->till_1_start }}</td>
                        <td>{{ $instant->till_2_start }}</td>
                        <td>{{ $instant->created_at }}</td>

                    </tr>
                    </tbody>
                    @endforeach
                </table>
           
            </div>
        </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">

    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection