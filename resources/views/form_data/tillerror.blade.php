@php

use App\Models\Tillerror;

$tillerrors = App\Models\Tillerror::all();
@endphp

@extends('layouts.blank')

@push('stylesheets')
        <!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="tile-stats">
                <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                <div class="count">{{ $tillerrors->count() }}</div>
                <h3>Till errors</h3>



            </div>
        </div>

    </div>

    <div class="row top_tiles">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; font-weight: bold">

            <table class="table .table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>t1 1</th>
                    <th>t1 2</th>
                    <th>t1 3</th>
                    <th>t1 4</th>
                    <th>t1 5</th>
                    <th>t1 6</th>
                    <th>t1 7</th>
                    <th>t1 note</th>
                    <th>Submitted</th>
                </tr>
                </thead>
                @foreach ($tillerrors as $tillerror)
                    <tbody>
                    <tr>
                        <td>{{ $tillerror->id }}</td>
                        <td>{{ $tillerror->employee_name }}</td>
                        <td>{{ $tillerror->till_1_1 }}</td>
                        <td>{{ $tillerror->till_1_2 }}</td>
                        <td>{{ $tillerror->till_1_3 }}</td>
                        <td>{{ $tillerror->till_1_4 }}</td>
                        <td>{{ $tillerror->till_1_5 }}</td>
                        <td>{{ $tillerror->till_1_6 }}</td>
                        <td>{{ $tillerror->till_1_7 }}</td>
                        <td>{{ $tillerror->till_1_note }}</td>
                        <td>{{ $tillerror->created_at }}</td>

                    </tr>
                    </tbody>
                @endforeach
            </table>

        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">

    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection