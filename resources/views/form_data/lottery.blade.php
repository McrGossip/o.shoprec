@php
use App\Models\Lottery;
$lotteries = App\Models\Lottery::all();
@endphp

@extends('layouts.blank')

@push('stylesheets')
        <!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="tile-stats">
                <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                <div class="count">{{ $lotteries->count() }}</div>
                <h3>Lottery</h3>
            </div>
        </div>
    </div>

    <div class="row top_tiles">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; font-weight: bold">

            <table class="table .table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>lm_1</th>
                    <th>lm_2</th>
                    <th>lm_3</th>
                    <th>lm_4</th>
                    <th>lm_5</th>
                    <th>lm_6</th>
                    <th>lm_7</th>
                    <th>lm_8</th>
                    <th>lm_9</th>
                    <th>lm_10</th>
                    <th>lm_11</th>
                    <th>Submitted</th>
                </tr>
                </thead>
                @foreach ($lotteries as $lottery)
                    <tbody>
                    <tr>
                        <td>{{ $lottery->id }}</td>
                        <td>{{ $lottery->employee_name }}</td>
                        <td>{{ $lottery->lm_1 }}</td>
                        <td>{{ $lottery->lm_2 }}</td>
                        <td>{{ $lottery->lm_3 }}</td>
                        <td>{{ $lottery->lm_4 }}</td>
                        <td>{{ $lottery->lm_5 }}</td>
                        <td>{{ $lottery->lm_6 }}</td>
                        <td>{{ $lottery->lm_7 }}</td>
                        <td>{{ $lottery->lm_8 }}</td>
                        <td>{{ $lottery->lm_9 }}</td>
                        <td>{{ $lottery->lm_10 }}</td>
                        <td>{{ $lottery->lm_11 }}</td>
                        <td>{{ $lottery->created_at }}</td>

                    </tr>
                    </tbody>
                @endforeach
            </table>

        </div>
    </div>

</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">

    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection