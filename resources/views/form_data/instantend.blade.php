<?php
use App\Models\Instantend;
$instantends = App\Models\Instantend::all();
?>
@extends('layouts.blank')

@push('stylesheets')
        <!-- Example -->
<!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="row top_tiles">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="tile-stats">
                <div class="icon"><i class="fa fa-comments-o"></i></div>
                <div class="count">{{ $instantends->count() }}</div>
                <h3>Instant ends</h3>


            </div>
        </div>

    </div>

<!-- /page content -->


    <div class="row top_tiles">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; font-weight: bold">

            <table class="table .table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>t1 end</th>
                    <th>t2 end</th>
                    <th>Submitted</th>
                </tr>
                </thead>
                @foreach ($instantends as $instantend)
                    <tbody>
                    <tr>
                        <td>{{ $instantend->id }}</td>
                        <td>{{ $instantend->employee_name }}</td>
                        <td>{{ $instantend->till_1_end }}</td>
                        <td>{{ $instantend->till_2_end }}</td>
                        <td>{{ $instantend->created_at }}</td>

                    </tr>
                    </tbody>

                @endforeach

            </table>

        </div>
    </div>
</div>
<!-- footer content -->
<footer>
    <div class="pull-right">

    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
@endsection