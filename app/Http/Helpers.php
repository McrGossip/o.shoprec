<?php
use App\Models\Instant as m_Instant;
use App\Models\Instantend as m_Instantend;
use App\Models\Lottery as m_Lottery;
use App\Models\Payzone as m_Payzone;
use App\Models\Tillerrors as m_Tillerrors;

function get_instant_count(){
    $instant_count=m_Instant::where('created_at', '>' , '2017-06-01')->count();
    return $instant_count;
}

function get_instantend_count(){
    $instantend_count=m_Instantend::where('created_at', '>' , '2017-06-01')->count();
    return $instantend_count;
}

function get_lottery_count(){
    $lottery_count=m_Lottery::where('created_at', '>' , '2017-06-01')->count();
    return $lottery_count;
}

function get_payzone_count(){
    $payzone_count=m_Payzone::where('active',1)->where('deleted',0)->where('product_list_id',9)->count();
    return $payzone_count;
}

function get_tillerrors_count(){
    $tillerrors_count=m_Tillerrors::where('active',1)->where('deleted',0)->count();
    return $tillerrors_count;
}