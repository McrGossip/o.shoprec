<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tillerror extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tillerrors';

}