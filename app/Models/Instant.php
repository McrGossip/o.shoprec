<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instant extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'instants';

}