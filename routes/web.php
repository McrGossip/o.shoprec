<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index');
Route::resource('user','UserController');
Route::get('user/{id}', 'UserController@showProfile');
Route::get('/form_data/instant', 'InstantController@index');
Route::get('/form_data/instantend', 'InstantendController@index');
Route::get('/form_data/lottery', 'LotteryController@index');
Route::get('/form_data/payzone', 'PayzoneController@index');
Route::get('/form_data/tillerror', 'TillerrorController@index');
